<?php
/**
 * Theme: Flat Bootstrap
 * 
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package flat-bootstrap
 */
?>
	</div><!-- #content -->


	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="upper-footer col-md-12">
			<div class="col-sm-4">
				<a href="/shop/variety-pack/">
					<div class="border-fix">
						<h3><i>Variety Pack</i></h3>
					</div>
					<img src="/wp-content/uploads/2018/03/steak-background.png">
				</a>
			</div>
			<div class="col-sm-4">
				<a href="/category/recipes/">
					<div class="border-fix">
						<h3><i>Recipes</i></h3>
					</div>
					<img src="/wp-content/uploads/2018/03/silverware-background.png">
				</a>
			</div>
			<div class="col-sm-4">
				<a href="/contact-us/">
					<div class="border-fix">
						<h3><i>Contact Us</i></h3>
					</div>
					<img src="/wp-content/uploads/2018/03/envelope-background.png">
				</a>
			</div>
		</div>
		<div class="lower-footer col-md-12">
			<div class="col-md-6">
			    <!-- social icons uncomment when ready
				<div class="col-sm-4 icons">
					<i class="fa fa-youtube-square"></i><i class="fa fa-facebook-square"></i><br>
					<i class="fa fa-twitter-square"></i><i class="fa fa-pinterest-square"></i>
				</div>
				-->
				<div class="col-sm-8 footer-left">
					<a href="mailto:info@nabison.com"><h3><b>info@nabison.com</b></h3></a>
					<h4>North American Bison, LLC</h4>
					<h5>1658 HWY 281 New Rockford, ND 58356</h5>
					<h4>800-289-2833</h4>
					<h5>&copy;<?php echo date('Y'); ?> North American Bison, LLC</h5>
				</div>
			</div>
			<div class="col-md-6">
				<div class="col-sm-4">
					<img src="/wp-content/uploads/2018/03/nab-small-logo.jpg">
					<img src="/wp-content/uploads/2018/03/tender-small-logo.jpg">
				</div>
				<div class="col-sm-8">
					<h5 style="text-align: right;line-height: 1.5;color: #fff;">Sign up for our eNewsletter</h5>
					<?php echo gravity_form( 1, false, false, false, '', false ); ?>
				</div>
			</div>
		</div>
		
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>
<script>
jQuery(document).ready(function() {
  
  jQuery(window).scroll(function () {
      //if you hard code, then use console
      //.log to determine when you want the 
      //nav bar to stick.  
      console.log(jQuery(window).scrollTop())
    if (jQuery(window).scrollTop() > 535) {
      jQuery('#masthead-home').addClass('navbar-fixed');
    }
    if (jQuery(window).scrollTop() < 536) {
      jQuery('#masthead-home').removeClass('navbar-fixed');
    }
  });
});
</script>
</body>
</html>