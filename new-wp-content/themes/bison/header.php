<?php
/**
 * Theme: Flat Bootstrap
 * 
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package flat-bootstrap
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<?php if(is_front_page()){ ?>
	<div class="col-md-12 home-header">
		<div style="position: absolute; z-index: -1;width:100%;height:100vh;top:-100;bottom:0;left:-100;right:0;">
			<video playsinline autoplay muted loop poster="/wp-content/uploads/2018/03/home-header.jpg" id="video">
		       <source src="/wp-content/uploads/2018/03/BisonSmall.mp4" type="video/mp4">
		        Your browser does not support the video tag. I suggest you upgrade your browser.
		    </video>
<!-- 			<iframe width="100%" height="100%" src="https://www.youtube.com/embed/RBEd3buAnwo?rel=0&autoplay=true&controls=0&showinfo=0&loop=1" frameborder="0" allow="autoplay; encrypted-media"></iframe> -->
		</div>
		<div class="main-logo"><img src="/wp-content/uploads/2018/03/main-logo.png" style="z-index: 2"></div>
		<div class="bottom-statement">
			<p><i>Enhancing the lives of people associated with Bison and Bison Products</i></p>
		</div>
	</div>
	<?php }; ?>
	
	<header id="masthead<?php if(is_front_page()){echo '-home';} ?>" class="col-md-12 site-header" role="banner">

		
		<nav id="site-navigation" class="main-navigation" role="navigation">

			<h2 class="menu-toggle screen-reader-text sr-only "><?php _e( 'Primary Menu', 'flat-bootstrap' ); ?></h2>
			<div class="skip-link"><a class="screen-reader-text sr-only" href="#content"><?php _e( 'Skip to content', 'flat-bootstrap' ); ?></a></div>

		<?php
		// Collapsed navbar menu toggle
		global $xsbf_theme_options;
		$navbar = '<div class="navbar ' . $xsbf_theme_options['navbar_classes'] . '">'
			.'<div class="container">'
        	.'<div class="navbar-header">'
          	.'<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">'
            .'<span class="icon-bar"></span>'
            .'<span class="icon-bar"></span>'
            .'<span class="icon-bar"></span>'
          	.'</button>';

		// Site title (Bootstrap "brand") in navbar. Hidden by default. Customizer will
		// display it if user turns off the main site title and tagline.
		$navbar .= '<a class="navbar-brand" href="'
			.esc_url( home_url( '/' ) )
			.'" rel="home"><img src="/wp-content/uploads/2018/03/main-logo.png">'
			.'</a>';
		
        $navbar .= '</div><!-- navbar-header -->';

		// Display the desktop navbar
		$navbar .= '<div class="navbar-collapse collapse">';
		$navbar .= wp_nav_menu( 
			array(  'theme_location' => 'primary',
			//'container_class' => 'navbar-collapse collapse', //<nav> or <div> class
			'menu_class' => 'nav navbar-nav', //<ul> class
			'walker' => new wp_bootstrap_navwalker(),
			'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
			'echo'	=> false
			) 
		);
				
		echo apply_filters( 'xsbf_navbar', $navbar );
		?>

		</div><!-- .container -->
		</div><!-- .navbar -->
		</nav><!-- #site-navigation -->

	</header><!-- #masthead -->

	<?php // Set up the content area (but don't put it in a container) ?>	
	<div id="content" class="site-content col-md-12">
		<div class="page-header-image">
			<?php if(!is_search() && !is_category() && !is_single() && !is_home() && !is_front_page()) { the_post_thumbnail( 'full' ); } ?>
		</div>
