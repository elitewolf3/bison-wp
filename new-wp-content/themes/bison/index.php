<?php
/**
 * Theme: Flat Bootstrap
 * 
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package flat-bootstrap
 */

get_header(); ?>
<?php if(!is_search()){; ?>
	<div class="col-md-12 category-main">
		<h1>Blog</h1>
		<p>Welcome to the Bison Bits blog—a resource for all things bison, including recipes for our delicious TenderBison cuts, cooking and grilling tips, stories about our incredible rancher partners, news from North American Bison, LLC, and more. </p>
	</div>
	<div class="col-md-12 category-bar">
		<?php echo wp_list_categories(array('title_li' => '')); ?>
	</div>
<?php }; ?>
<div class="container">
<div id="main-grid">

	<section id="primary" class="content-area col-md-8">
		<main id="main" class="site-main" role="main">
		<div class="blog-loop">
		<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
					<div class="col-md-4 blog-full">
						<div class="blog-single">
							<?php the_post_thumbnail('medium'); ?>
							<h3 class="cat-title">
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<div class="entry-summary">
							<?php echo get_the_excerpt(); ?>
							<hr>
							</div>
						</div>
					</div>

			<?php endwhile; ?>
		</div>
			<?php get_template_part( 'content', 'index-nav' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'index' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

	<?php get_sidebar(); ?>

</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>