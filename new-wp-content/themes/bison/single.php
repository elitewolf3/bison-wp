<?php
/**
 * Theme: Flat Bootstrap
 * 
 * The Template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package flat-bootstrap
 */

get_header(); ?>

<div class="container">
<div id="main-grid" class="row">

	<div id="primary" class="content-area col-md-8">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<?php if(in_category( '3' )){ ;?>
			<div class="col-md-7">
				<?php the_post_thumbnail( 'large' ); ?>
				<h1 class="single-blog-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
			</div>
			<div class="col-md-5">
				<h2><b>Ingredients</b></h2>
				<?php $value = get_field( "ingredients" ); 
				if( $value ) {
    				echo $value;
				}; ?>
			</div>
			<?  } else { ;?>
			<div class="col-md-6">
				<h1 class="single-blog-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
			</div>
			<div class="col-md-6">
				<?php the_post_thumbnail( 'large' ); ?>
			</div>
				<?php }; ?>
			<div class="col-md-12 pad-content">
				<?php the_content(); ?>
			</div>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() )
					comments_template();
			?>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>