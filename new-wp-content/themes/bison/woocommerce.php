<?php
/**
 * Theme: Flat Bootstrap
 * 
 *
 * This is the template for full width pages with no sidebar, no container, and
 * no page header. This page truly stretches the full width of the browser window. 
 * You should set a <div class="container"> before your content to keep it in line 
 * with the rest of the site content.
 *
 * @package flat-bootstrap
 */

get_header(); ?>

<div id="primary" class="content-area-wide">
	<main id="main" class="site-main" role="main">

<?php woocommerce_content(); ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
